from django.apps import AppConfig


class CurrencytickerConfig(AppConfig):
    name = 'currencyticker'

    def ready(self):
        from currencyticker import tools
        tools.start_renewal_currencies()
