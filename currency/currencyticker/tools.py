# -*- coding:utf-8 -*-


__all__ = [
    'start_renewal_currencies',
]


import requests
import json
import threading
import time

from .models import Currency, CurrencyEntry


URL_CURRENCY_SERVICE = 'https://api.cryptonator.com/api/ticker/{base}-{target}'


def get_currency(base, target):
    return json.loads(requests.get(URL_CURRENCY_SERVICE.format(base=base, target=target)).text)

def renewal_currencies():
    for i in Currency.objects.all():
        response = get_currency(base=i.base, target=i.target)
        CurrencyEntry.objects.create(currency_value=round(float(response['ticker']['price']), 2), currency=i)

def auto_renewal_currencies(every):
    while True:  # Это ужасно, можно было использовать schedule или celery, но ратировали за простоту!
        time.sleep(every)
        renewal_currencies()


def start_renewal_currencies(every=10):
    thrd = threading.Thread(target=auto_renewal_currencies, args=[every])
    thrd.setDaemon(True)
    thrd.start()
