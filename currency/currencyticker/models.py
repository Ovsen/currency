# -*- coding:utf-8 -*-


__all__ = [
    'Currency',
    'CurrencyEntry',
]


from django.db import models


class Currency(models.Model):
    base = models.CharField(max_length=5)
    target = models.CharField(max_length=5)
    dt_created = models.DateTimeField(auto_now_add=True)
    dt_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'base: {0}, target: {1}'.format(self.base, self.target)


class CurrencyEntry(models.Model):
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    currency_value = models.FloatField()
    dt_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{0}: {1} - {2}'.format(self.currency, self.currency_value, self.dt_created)
