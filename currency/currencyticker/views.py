from django.shortcuts import render, redirect
from django.urls import reverse

from .models import Currency
from .forms import CurrencyForm


def main_page(request):
    currencies = Currency.objects.all()
    currencyform = CurrencyForm()
    context = {'currencies': currencies, 'currencyform': currencyform}
    return render(request, 'currencyticker/index.html', context)


def create_currency(request):
    currencyform = CurrencyForm(request.GET)
    if currencyform.is_valid():
        currencyform.save()
    return redirect(reverse('main-page'))
