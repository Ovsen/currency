# -*- coding:utf-8 -*-


__all__ = [
    'CurrencyForm',
]


from django.forms import ModelForm

from .models import Currency


class CurrencyForm(ModelForm):
    class Meta:
        model = Currency
        fields = ['base', 'target']
